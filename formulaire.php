<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = htmlspecialchars(trim($_POST['name']));
    $email = htmlspecialchars(trim($_POST['email']));
    $message = htmlspecialchars(trim($_POST['message']));

    if (!empty($name) && !empty($email) && !empty($message)) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

            $to = "votre-email@example.com"; 
            $subject = "Nouveau message de contact de $name";
            $body = "Nom: $name\nEmail: $email\n\nMessage:\n$message";
            $headers = "From: $email\r\n";
            $headers .= "Reply-To: $email\r\n";
            $headers .= "X-Mailer: PHP/" . phpversion();
            if (mail($to, $subject, $body, $headers)) {
                echo "Votre message a été envoyé avec succès.";
            } else {
                echo "Une erreur s'est produite lors de l'envoi du message. Veuillez réessayer.";
            }

        } else {
            echo "L'adresse e-mail est invalide.";
        }
    } else {
        echo "Veuillez remplir tous les champs.";
    }
} else {
    echo "Méthode non autorisée.";
}
?>